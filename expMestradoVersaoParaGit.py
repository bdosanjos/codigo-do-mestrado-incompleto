# -*- coding: utf-8 -*-
"""
Created on Thu Jun 10 14:06:30 2021

@author: Bruno
"""
import hyperopt 
from hyperopt import Trials, STATUS_OK, tpe
import gc
import keras
from keras.datasets import mnist
from keras.layers import Input, Dense, Reshape, Flatten, Dropout, MaxPooling2D
from keras.layers import BatchNormalization, Activation, ZeroPadding2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import UpSampling2D, Convolution2D
from keras.models import Sequential, Model
from keras.optimizers import RMSprop
from keras.preprocessing import image
import os
from keras import layers
import keras.backend as K
from sklearn.metrics import auc

from keras import applications
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.layers import  GlobalAveragePooling2D
from keras import backend as k 
from keras.layers import concatenate
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, TensorBoard, EarlyStopping
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve
from sklearn.utils import shuffle
import sys
from sklearn.metrics import classification_report
import numpy as np
from keras.preprocessing.image import ImageDataGenerator
from skimage import color
import matplotlib.pyplot as plt

from contextlib import redirect_stdout
from hyperas.distributions import choice, uniform, quniform

from hyperas import optim

import gc
import keras
from keras.layers import Input, Dense, Reshape, Flatten, Dropout,MaxPooling2D
from keras.layers import BatchNormalization, Activation, ZeroPadding2D
from keras.layers.advanced_activations import LeakyReLU
from keras.layers.convolutional import UpSampling2D, Conv2D
from keras.models import Sequential, Model
from keras.optimizers import RMSprop
from keras.preprocessing import image
import os
from keras import layers
import keras.backend as K
from sklearn.metrics import auc
from keras import applications
from keras.preprocessing.image import ImageDataGenerator
from keras import optimizers
from keras.layers import  GlobalAveragePooling2D
from keras import backend as k 
from keras.layers import concatenate
from keras.callbacks import ModelCheckpoint, LearningRateScheduler, TensorBoard, EarlyStopping
from sklearn.metrics import accuracy_score
import matplotlib.pyplot as plt
from sklearn.metrics import confusion_matrix
from sklearn.metrics import roc_curve
from sklearn.utils import shuffle
import sys
from sklearn.metrics import classification_report
import numpy as np
import glob 
from skimage import color

from contextlib import redirect_stdout
#%
from tensorflow.python.client import device_lib
import tensorflow as tf
tf.keras.backend.clear_session()
#--------- funcao de load data---------------------#

#buscando dados em uma pasta
def encontraArquivosEmPastaRecursivamente(pasta, extensao):
    arquivosTxt = []
    caminhoAbsoluto = os.path.abspath(pasta)
    for pastaAtual, subPastas, arquivos  in os.walk(caminhoAbsoluto):
        arquivosTxt.extend([os.path.join(pastaAtual,arquivo) for arquivo in arquivos if arquivo.endswith(extensao)])
    return arquivosTxt

#Esticando as imagens para o mesmo minimo e maximo de intensidade de pixel

def esticando(imgs,maximo):
    dup = [] 
    for ka in imgs:
        for i in ka:
            dup.append(i)
    mini = min(dup)
    imgs = (imgs- mini)*(1.+(mini/255))

    return imgs


#Carregando as imagens e seus respectvos labels em preto e branco (canal =1)
def carregarImagens(caminho,height,width,bORm):
    nodulos = encontraArquivosEmPastaRecursivamente(caminho,'.png')
    imagensCarregadas = np.zeros((len(nodulos), height,width))
    labels =[]
    global limiar
    for i in range(len(imagensCarregadas)):
        aux = image.load_img(nodulos[i],target_size=(height,width), grayscale=True)
        limiar =0
        imagensCarregadas[i] = esticando(np.asarray(aux).astype('float32'),255)
        #imagensCarregadas[i] =   color.gray2rgb(aux)
        if(bORm ==0):
            labels.append(0)
        else:
            labels.append(1)
        
    return imagensCarregadas, labels

#----------------------------------Aquitetura das GANS-------------------------------#

def generator(ruidoDim=100, canal=1):
    global mainPath
    model = Sequential()

    model.add(Dense(256 * 5* 5, activation="relu", input_dim=ruidoDim))
    model.add(Reshape((5, 5, 256)))
    model.add(UpSampling2D())
    model.add(Conv2D(128, kernel_size=5, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(Activation("relu"))
    model.add(UpSampling2D())
    model.add(Conv2D(64, kernel_size=5, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(Activation("relu"))
    model.add(UpSampling2D())
    model.add(Conv2D(32, kernel_size=5, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(Activation("relu"))
    model.add(Conv2D(canal, kernel_size=5, padding="same"))
    
    model.add(Activation("tanh"))

    #model.summary()


    noise = Input(shape=(ruidoDim,))
    img = model(noise)

    return Model(noise, img)

#%Arquitetura do Discriminante GAN para DCGAN
def discriminante(altura=40, largura=40, canal=1):
    global mainPath
    model = Sequential()

    model.add(Conv2D(64, kernel_size=5, strides=2, input_shape=(altura, largura, canal), padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(LeakyReLU())
    model.add(Dropout(0.6))
    model.add(Conv2D(128, kernel_size=5, strides=2, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(LeakyReLU())
    model.add(Dropout(0.6))
    model.add(Conv2D(256, kernel_size=5, strides=2, padding="same"))
    model.add(BatchNormalization(momentum=0.8))
    model.add(LeakyReLU())
    model.add(Dropout(0.6))
    model.add(Flatten())
    model.add(Dense(1,activation='sigmoid'))

   # model.summary()



    img = Input((altura, largura, canal))
    validity = model(img)

    return Model(img, validity)


#-----------Aumento de base Classic-------------#
def dataArg(imagens, rotacao,c):
        
    imgAug = ImageDataGenerator(rotation_range=rotacao, width_shift_range=0.1,
                                height_shift_range=0.1, zoom_range=0.15,
                                fill_mode='nearest', horizontal_flip=True)
    
    return imgAug






#------------Divisao em k folds--------------#
def kfoldcv(indices, k):
    
    size = len(indices)
    subset_size = round(size / k)
    
    subsets = [indices[x:x+subset_size] for x in range(0, len(indices), subset_size)]
    kfolds = []
    for i in range(k):
        test = subsets[i]
        train = []
        for subset in subsets:
            if subset != test:
                train.append(subset)
        kfolds.append((train,test))
        
    return kfolds






#---------------Parametros-------------------#
#tamanho da imagem
tamImag="40"
height = int(tamImag)
width = int(tamImag)
channels = 1
#caminho para nodulos segmentados
SegB = 'Caminho para os nódulos benignos Segmentados'
SegM = 'Caminho para os nódulos malignos Segmentados'


#caminho para nodulos
train_dir_B ='Caminho para os nódulos benignos'
train_dir_M ='Caminho para os nódulos malignos'


mainPath ='Caminho pra onde os resultados vão ser salvos'

vgan = "Vanilla GAN"
afim = "Classic"
dcgan = "DCGAN"
wgan = "WGAN"
anjosGAN = "AnjosGAN"

latent_dim = 100
batch_sizeB =63
batch_sizeM = 50
folds = 6
InteracaiArgClassic = 4

treino=0
treinoLabel= 0
teste=0
testeLabel=0
validacao =0
validacaoLabel =0



#------------------Load data----------#
#Os nódulos são separados em pastas, logo a divisção do que vai para treino, validação e teste é feito com divisção nas pastas
#dessa forma evita que tenha imagens diferentes do mesmo nódulo no treino e no teste.
  #buscando lista de nodulos benignos
listaBenigno =[]   
listaSegmentados =[]  
for i in glob.glob(train_dir_B+"/*"):    

    listaBenigno.append(i)
     
for i in glob.glob(SegB+"/*"):      
     listaSegmentados.append(i)
        
#buscando lista de nodulos malignos      
listaMaligno =[]   
listaSegmentadosM =[]  
for i in glob.glob(train_dir_M+"/*"):      
    listaMaligno.append(i)
     
for i in glob.glob(SegM+"/*"):      
    listaSegmentadosM.append(i)
    
    
#----------Execuçção do Classificador-------------------#

def runArgclassic(fold,nB,nB2,lnB,lnB2,validacaoB,labelsValB,nM,nM2,lnM,lnM2,validacaoM,labelsValM,dataArgM,dataArgB,dataArgtipo):
    
    
    treinoB = np.concatenate([nB,dataArgB])
    treinoBlab = np.concatenate([lnB,np.zeros(len(dataArgB))])
    treinoM = np.concatenate([nM,dataArgM])
    treinoMlab = np.concatenate([lnM,np.ones(len(dataArgM))])
    

    treino = np.concatenate([treinoB,treinoM])
    treinoLabel = np.concatenate([treinoBlab,treinoMlab])
    
    validacao =np.concatenate([validacaoB,validacaoM])
    validacao =  validacao.reshape(( validacao.shape[0],) +(height,width, channels)).astype('float32') / 255.
    validacaoLabel =np.concatenate([labelsValB,labelsValM])
    
    
    if len(nB2) > len(nM2):
        testeDif = len(nM2)
        teste = np.concatenate([nB2[:testeDif],nM2])
        test_labels = np.concatenate([lnB2[:testeDif],lnM2])
    else:
        testeDif = len(nB2)
        teste = np.concatenate([nB2,nM2[:testeDif]])
        test_labels = np.concatenate([lnB2,lnM2[:testeDif]])
    
    teste =  teste.reshape(( teste.shape[0],) +(height,width, channels)).astype('float32') / 255.

    
    
    global mainPath
    inputA = Input(shape=(height,width,channels))
    kernel_size = 3
    m = Conv2D(256, kernel_size=kernel_size)(inputA)
    m = BatchNormalization()(m)
    m = LeakyReLU()(m)
    m = Dropout(0.4)(m)
    m = MaxPooling2D(pool_size=(2, 2))(m)
    
    q = Conv2D(128, kernel_size=kernel_size)(m)
    q = BatchNormalization()(q)
    q = LeakyReLU()(q)
    q = Dropout(0.4)(q)
    q = MaxPooling2D(pool_size=(2, 2))(q)
    
    s = Conv2D(64, kernel_size=kernel_size)(q)
    s = BatchNormalization()(s)
    s = LeakyReLU()(s)
    s = Dropout(0.4)(s)
    
 
   
    x = Flatten()(s) 
    x = Dense(2048, activation="relu")(x)
    s = Dropout(0.5)(s)
    x = Dense(2048, activation="relu")(x)
    predictions = Dense(1, activation="sigmoid")(x)

    model_final = Model(input = (inputA), output = predictions)
   # model_final.summary()
    
    # compile the model 
    model_final.compile(loss = "binary_crossentropy", optimizer = optimizers.Adam(
    lr=0.00001), metrics=["accuracy"])
    
  
    
    paraCedo = EarlyStopping(monitor='val_loss',min_delta=0,patience =1, mode='auto')
    model_final.fit( treino, treinoLabel, epochs=200,batch_size=50, validation_data=(validacao, validacaoLabel),shuffle=True,callbacks =[paraCedo])
   
  
    y_pred_keras = model_final.predict(teste).ravel()
    y_pred_keras_int=[]
    for i in range(len(y_pred_keras)):
        if y_pred_keras[i]>=0.5:
            y_pred_keras_int.append(1)
        else:
            y_pred_keras_int.append(0)
            
    file = open(mainPath+"/vetores"+str(dataArgtipo)+".txt","a+") 
    file.write(str(fold))
    file.write("\n")
    file.write("$Predição:$")
    file.write("\n")
    file.write(str(y_pred_keras_int))
    file.write("\n")
    file.write("$Labels Reais:$")
    file.write("\n")
    file.write(str(test_labels))
    file.write("$------------------------------------------$\n")     
   
    print(classification_report(test_labels, y_pred_keras_int))      
   
    fpr_keras, tpr_keras, thresholds_keras = roc_curve(test_labels, y_pred_keras)
    
    auc_keras = auc(fpr_keras, tpr_keras)

    file = open(mainPath+"/resultados"+str(fold)+".txt","a+") 
    file.write(classification_report(test_labels, y_pred_keras_int))
    file.write("\n")
    file.write(str(confusion_matrix(test_labels, y_pred_keras_int)))
    file.write("\n")
    file.write("Acc: "+str(accuracy_score(test_labels, y_pred_keras_int))+"\n")
    file.write(dataArgtipo+" %.3f\n" % (auc_keras))
    file.write("------------------------------------------\n")
   
    file.close()         
   
    fpr_keras, tpr_keras, thresholds_keras = roc_curve(test_labels, y_pred_keras)
    
    auc_keras = auc(fpr_keras, tpr_keras)

    tf.keras.backend.clear_session()
    gc.collect()
    
    plt.figure(1)
    plt.plot([0, 1], [0, 1], 'k--')
    plt.plot(fpr_keras, tpr_keras, label=dataArgtipo+' (area = {:.3f})'.format(auc_keras))
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve')
    plt.legend(loc='best')
    plt.savefig(mainPath+"/"+dataArgtipo+" "+str(fold))
    plt.show()
    
    
    return fpr_keras, tpr_keras, auc_keras

#-----------------------AnjosGAN------------##
#Treinamento em dois momentos, primeiro aprender a fazer nódulos segmentados e depois aprende nódulos com as estruturas  adjacentes.
#Chamda de PREDCGAN na tese
def AnjosGAN(nB,segB,nM,segM,aumentoB,aumentoM):
    
    interacoesB = 80001
    #interacoesB = 2

    lrG = 0.001
    lrD = 0.0001

    realB = segB
    realM = segM

    #%
    validMM= np.zeros((batch_sizeM, 1))
    fakeM = np.ones((batch_sizeM, 1))

    #%
    criticB = discriminante(height,width,channels)
    #Metricas de compilação
    criticB.compile(loss='binary_crossentropy',optimizer= keras.optimizers.Adam(lrD,0.5), metrics=['accuracy'])
    generatorB = generator(latent_dim,channels)
    #Montando a rede
    #Gerador com entrada do vetor ruído
    noise = Input(shape=(latent_dim,))

    img = generatorB(noise)
    criticB.trainable = False

    validB = criticB(img)

    combinedB = Model(noise, validB)
    #compilando a Rede
    combinedB.compile(loss='binary_crossentropy',optimizer=keras.optimizers.Adam(lrG,0.5), metrics=['accuracy'])


    for epoch in range(interacoesB):
        gc.collect()
        #treinamento das GAN Benignas
        idx = np.random.randint(0, realB.shape[0], batch_sizeB)
        imgsB = realB[idx]

        noise = np.random.normal(0, 1, (batch_sizeB, latent_dim))

        
        gen_imgsB = generatorB.predict(noise)
        validBB = np.zeros((batch_sizeB, 1))
        fakeB = np.ones((batch_sizeB, 1))

        d_loss_realB = criticB.train_on_batch(imgsB, validBB)
        d_loss_fakeB = criticB.train_on_batch(gen_imgsB, fakeB)
        d_lossB = 0.5 * np.add(d_loss_fakeB, d_loss_realB)
        #%

        g_lossB = combinedB.train_on_batch(noise, validBB)

        print ("AnjosGANS Benigno: %d [Discr loss: %f] [Gera loss: %f] " % (epoch, 1 - d_lossB[0], 1 - g_lossB[0])) 

        
        
    interacoesB = 80001
    #interacoesB = 2
    del realB
    
    realB = nB


    validMM= np.zeros((batch_sizeM, 1))
    fakeM = np.ones((batch_sizeM, 1))

    for epoch in range(interacoesB):
        gc.collect()
        #treinamento das GAN Benignas
        idx = np.random.randint(0, realB.shape[0], batch_sizeB)
        imgsB = realB[idx]

        noise = np.random.normal(0, 1, (batch_sizeB, latent_dim))

        # Generate a batch of new images
        gen_imgsB = generatorB.predict(noise)
        validBB = np.zeros((batch_sizeB, 1))
        fakeB = np.ones((batch_sizeB, 1))
        # Train the critic
        d_loss_realB = criticB.train_on_batch(imgsB, validBB)
        d_loss_fakeB = criticB.train_on_batch(gen_imgsB, fakeB)
        d_lossB = 0.5 * np.add(d_loss_fakeB, d_loss_realB)
        #%

        g_lossB = combinedB.train_on_batch(noise, validBB)


        print ("AnjosGAN Benigno: %d [Discr loss: %f] [Gera loss: %f] " % (epoch, 1 - d_lossB[0], 1 - g_lossB[0])) 

    random_latent_vectors = np.random.normal(0, 1, (aumentoB, latent_dim))       
    imagens_ganB = generatorB.predict(random_latent_vectors)    
    
    #TREINANDO a Gan maligna
    #%
    realM = segM
    interacoesM = 80001
    #interacoesM = 2

    #%
    validMM= np.zeros((batch_sizeM, 1))
    fakeM = np.ones((batch_sizeM, 1))

    criticM = discriminante(height,width,channels)
    #Metricas de compilação
    criticM.compile(loss='binary_crossentropy',optimizer= keras.optimizers.Adam(lrD,0.5), metrics=['accuracy'])
    generatorM = generator(latent_dim,channels)
    #Montando a rede
    #Gerador com entrada do vetor ruído
    noise = Input(shape=(latent_dim,))

    img = generatorM(noise)

    criticM.trainable = False

    validM = criticM(img)

    combinedM = Model(noise, validM)
    #compilando a Rede
    combinedM.compile(loss='binary_crossentropy',optimizer=keras.optimizers.Adam(lrG,0.5), metrics=['accuracy'])


    #%

    for epoch in range(interacoesM):
        gc.collect()
        #treinamento das GAN Benignas
        idx = np.random.randint(0, realM.shape[0], batch_sizeM)
        imgsM = realM[idx]

        noise = np.random.normal(0, 1, (batch_sizeM, latent_dim))


        gen_imgsM = generatorM.predict(noise)
        validMM = np.zeros((batch_sizeM, 1))
        fakeM = np.ones((batch_sizeM, 1))

        d_loss_realM = criticM.train_on_batch(imgsM, validMM)
        d_loss_fakeM = criticM.train_on_batch(gen_imgsM, fakeM)
        d_lossM = 0.5 * np.add(d_loss_fakeM, d_loss_realM)
        #%

        g_lossM = combinedM.train_on_batch(noise, validMM)


        print ("AnjosGANS Maligno: %d [Discr loss: %f] [Gera loss: %f] " % (epoch, 1 - d_lossM[0], 1 - g_lossM[0]))

    del realM
    realM = nM
    interacoesM = 80001
    #interacoesM = 2

    #%
    validMM= np.zeros((batch_sizeM, 1))
    fakeM = np.ones((batch_sizeM, 1))
   
    compl = 0
    for epoch in range(interacoesM):
        gc.collect()
        #treinamento das GAN Benignas
        idx = np.random.randint(0, realM.shape[0], batch_sizeM)
        imgsM = realM[idx]

        noise = np.random.normal(0, 1, (batch_sizeM, latent_dim))



        # Generate a batch of new images
        gen_imgsM = generatorM.predict(noise)
        validMM = np.zeros((batch_sizeM, 1))
        fakeM = np.ones((batch_sizeM, 1))
        # Train the critic
        d_loss_realM = criticM.train_on_batch(imgsM, validMM)
        d_loss_fakeM = criticM.train_on_batch(gen_imgsM, fakeM)
        d_lossM = 0.5 * np.add(d_loss_fakeM, d_loss_realM)
        #%

        g_lossM = combinedM.train_on_batch(noise, validMM)
      


        print ("AnjosGAN Maligno: %d [Discr loss: %f] [Gera loss: %f] " % (epoch, 1 - d_lossM[0], 1 - g_lossM[0]))

    random_latent_vectors = np.random.normal(0, 1, (aumentoM, latent_dim))#np.random.normal(size=(gan_number,latent_dim))    

    imagens_ganM = generatorM.predict(random_latent_vectors)  


    tf.keras.backend.clear_session()
    gc.collect()
    
    return  imagens_ganB, imagens_ganM


#-----------------------DCGAN------------------------#
def DCGAN(nB,nM,aumentoB,aumentoM):
                                                 
    #%Rodando as GANS

    interacoesB =160001
    #interacoesB =2
    interacoesM = 160001
    #interacoesM = 2
    lrG = 0.001
    lrD = 0.0001

    realB = nB
    realM = nM

    #%
    validMM= np.zeros((batch_sizeM, 1))
    fakeM = np.ones((batch_sizeM, 1))

    criticB = discriminante(height,width,channels)
    #Metricas de compilação
    criticB.compile(loss='binary_crossentropy',optimizer= keras.optimizers.Adam(lrD,0.5), metrics=['accuracy'])
    generatorB = generator(latent_dim,channels)
    #Montando a rede
    #Gerador com entrada do vetor ruído
    noise = Input(shape=(latent_dim,))

    img = generatorB(noise)

    criticB.trainable = False

    validB = criticB(img)

    combinedB = Model(noise, validB)
    #compilando a Rede
    combinedB.compile(loss='binary_crossentropy',optimizer=keras.optimizers.Adam(lrG,0.5), metrics=['accuracy'])

    for epoch in range(interacoesB):
        gc.collect()
        #treinamento das GAN Benignas
        idx = np.random.randint(0, realB.shape[0], batch_sizeB)
        imgsB = realB[idx]

        noise = np.random.normal(0, 1, (batch_sizeB, latent_dim))


        gen_imgsB = generatorB.predict(noise)
        validBB = np.zeros((batch_sizeB, 1))
        fakeB = np.ones((batch_sizeB, 1))

        d_loss_realB = criticB.train_on_batch(imgsB, validBB)
        d_loss_fakeB = criticB.train_on_batch(gen_imgsB, fakeB)
        d_lossB = 0.5 * np.add(d_loss_fakeB, d_loss_realB)
        #%

        g_lossB = combinedB.train_on_batch(noise, validBB)

        print ("DC Benigno: %d [Discr loss: %f] [Gera loss: %f] " % (epoch, 1 - d_lossB[0], 1 - g_lossB[0])) 
       
    random_latent_vectors = np.random.normal(0, 1, (aumentoB, latent_dim))#np.random.normal(size=(gan_number,latent_dim))         
    imagens_ganB = generatorB.predict(random_latent_vectors)  
  

    #%
    validMM= np.zeros((batch_sizeM, 1))
    fakeM = np.ones((batch_sizeM, 1))

    criticM = discriminante(height,width,channels)
    #Metricas de compilação
    criticM.compile(loss='binary_crossentropy',optimizer= keras.optimizers.Adam(lrD,0.5), metrics=['accuracy'])
    generatorM = generator(latent_dim,channels)
    #Montando a rede
    #Gerador com entrada do vetor ruído
    noise = Input(shape=(latent_dim,))

    img = generatorM(noise)

    criticM.trainable = False

    validM = criticM(img)

    combinedM = Model(noise, validM)
    #compilando a Rede
    combinedM.compile(loss='binary_crossentropy',optimizer=keras.optimizers.Adam(lrG,0.5), metrics=['accuracy'])


    #%
    validMM= np.zeros((batch_sizeM, 1))
    fakeM = np.ones((batch_sizeM, 1))


    for epoch in range(interacoesM):
        gc.collect()
        #treinamento das GAN Benignas
        idx = np.random.randint(0, realM.shape[0], batch_sizeM)
        imgsM = realM[idx]

        noise = np.random.normal(0, 1, (batch_sizeM, latent_dim))

        # Generate a batch of new images
        gen_imgsM = generatorM.predict(noise)
        validMM = np.zeros((batch_sizeM, 1))
        fakeM = np.ones((batch_sizeM, 1))
        # Train the critic
        d_loss_realM = criticM.train_on_batch(imgsM, validMM)
        d_loss_fakeM = criticM.train_on_batch(gen_imgsM, fakeM)
        d_lossM = 0.5 * np.add(d_loss_fakeM, d_loss_realM)
        #%

        g_lossM = combinedM.train_on_batch(noise, validMM)


        print ("DC Maligno: %d [Discr loss: %f] [Gera loss: %f] " % (epoch, 1 - d_lossM[0], 1 - g_lossM[0]))

    random_latent_vectors = np.random.normal(0, 1, (aumentoM, latent_dim))#np.random.normal(size=(gan_number,latent_dim))    

    imagens_ganM = generatorM.predict(random_latent_vectors)  
    
    tf.keras.backend.clear_session()
    gc.collect()
                                                 
    return imagens_ganB,imagens_ganM

#------------------------------WGAN-------------------------------#
def WGAN(nB,nM,aumentoB,aumentoM):
    
    def wasserstein_loss(y_true, y_pred):
        return K.mean(y_true * y_pred)
    n_critic = 5
    clip_value = 0.01

                                                 
    #%Rodando as GANS

    interacoesB =200001
    #interacoesB =2
    interacoesM = 200001
    #interacoesM = 2
    lrG = 0.00005
    lrD = 0.00005

    realB = nB
    realM = nM

    criticB = discriminante(height,width,channels)
    #Metricas de compilação
    criticB.compile(loss=wasserstein_loss,optimizer= keras.optimizers.RMSprop(lr=lrD), metrics=['accuracy'])
    generatorB = generator(latent_dim,channels)
    #Montando a rede
    #Gerador com entrada do vetor ruído
    noise = Input(shape=(latent_dim,))

    img = generatorB(noise)

    criticB.trainable = False

    validB = criticB(img)

    combinedB = Model(noise, validB)
    #compilando a Rede
    combinedB.compile(loss=wasserstein_loss,optimizer=keras.optimizers.RMSprop(lr=lrG), metrics=['accuracy'])

    for epoch in range(interacoesB):
        gc.collect()
        #treinamento das GAN Benignas
        idx = np.random.randint(0, realB.shape[0], batch_sizeB)
        imgsB = realB[idx]

        noise = np.random.normal(0, 1, (batch_sizeB, latent_dim))


        gen_imgsB = generatorB.predict(noise)
        validBB = -np.ones((batch_sizeB, 1))
        fakeB = np.ones((batch_sizeB, 1))

        d_loss_realB = criticB.train_on_batch(imgsB, validBB)
        d_loss_fakeB = criticB.train_on_batch(gen_imgsB, fakeB)
        d_lossB = 0.5 * np.add(d_loss_fakeB, d_loss_realB)
        #%
        for l in criticB.layers:
                weights = l.get_weights()
                weights = [np.clip(w, -clip_value, clip_value) for w in weights]
                l.set_weights(weights)

        g_lossB = combinedB.train_on_batch(noise, validBB)

        print ("WGAN Benigno: %d [Discr loss: %f] [Gera loss: %f] " % (epoch, 1 - d_lossB[0], 1 - g_lossB[0])) 
       
    random_latent_vectors = np.random.normal(0, 1, (aumentoB, latent_dim))#np.random.normal(size=(gan_number,latent_dim))         
    imagens_ganB = generatorB.predict(random_latent_vectors)  
  


    criticM = discriminante(height,width,channels)
    #Metricas de compilação
    criticM.compile(loss=wasserstein_loss,optimizer= keras.optimizers.RMSprop(lr=lrD), metrics=['accuracy'])
    generatorM = generator(latent_dim,channels)
    #Montando a rede
    #Gerador com entrada do vetor ruído
    noise = Input(shape=(latent_dim,))

    img = generatorM(noise)

    criticM.trainable = False

    validM = criticM(img)

    combinedM = Model(noise, validM)
    #compilando a Rede
    combinedM.compile(loss=wasserstein_loss,optimizer=keras.optimizers.RMSprop(lr=lrG), metrics=['accuracy'])



    for epoch in range(interacoesM):
        idx = np.random.randint(0, realM.shape[0], batch_sizeM)
        imgsM = realM[idx]

        noise = np.random.normal(0, 1, (batch_sizeM, latent_dim))

        # Generate a batch of new images
        gen_imgsM = generatorM.predict(noise)
        validMM = -np.ones((batch_sizeM, 1))
        fakeM = np.ones((batch_sizeM, 1))
        # Train the critic
        d_loss_realM = criticM.train_on_batch(imgsM, validMM)
        d_loss_fakeM = criticM.train_on_batch(gen_imgsM, fakeM)
        d_lossM = 0.5 * np.add(d_loss_fakeM, d_loss_realM)
        #%

        g_lossM = combinedM.train_on_batch(noise, validMM)
        for l in criticM.layers:
            weights = l.get_weights()
            weights = [np.clip(w, -clip_value, clip_value) for w in weights]
            l.set_weights(weights)


        print ("WGAN Maligno: %d [Discr loss: %f] [Gera loss: %f] " % (epoch, 1 - d_lossM[0], 1 - g_lossM[0]))

    random_latent_vectors = np.random.normal(0, 1, (aumentoM, latent_dim))#np.random.normal(size=(gan_number,latent_dim))    

    imagens_ganM = generatorM.predict(random_latent_vectors)  
    
    tf.keras.backend.clear_session()
    gc.collect()
                                                 
    return imagens_ganB,imagens_ganM


#----------------------------execução--------------#




def xecutar(listaBenigno,listaSegmentados,listaMaligno,listaSegmentadosM,folds):
    #classic
    fp1 = [] 
    tp1 = [] 
    auc1 =[]
    #anjosGAN
    fp2 = [] 
    tp2 = [] 
    auc2 =[]
    #classic + anjosGAN
    fp3 = [] 
    tp3 = [] 
    auc3 =[]
    #DCGAN
    fp4 = [] 
    tp4 = [] 
    auc4 =[]
    #classic + DCGAN
    fp5 = [] 
    tp5 = [] 
    auc5 =[]
    #WGAN
    fp6 = [] 
    tp6 = [] 
    auc6 =[]
    #classic + WGAN
    fp7 = [] 
    tp7 = [] 
    auc7 =[]
    foldsDivididas = kfoldcv(listaBenigno,folds)
    foldsDivididasSeg = kfoldcv(listaSegmentados,folds)
    
    foldsDivididasM = kfoldcv(listaMaligno,folds)
    foldsDivididasSegM = kfoldcv(listaSegmentadosM,folds)
   #___________________________________________
    menorTeste= 100000000
    menorvali= 100000000
    menortreino= 100000000
    menorTesteM= 100000000
    menorvaliM= 100000000
    menortreinoM= 100000000
    for h in range(folds):
        testeListaBenigno = foldsDivididas[h][1]
        
        testeListaMaligno = foldsDivididasM[h][1]
        for j in range(folds-1):
            if j==0:
                validacaoListaBenigno = foldsDivididas[h][0][j]
                
                validacaoListaMaligno = foldsDivididasM[h][0][j]
            elif j==1: 
                treinoListaBenigno = foldsDivididas[h][0][j]
                treinoListaBenignoSeg = foldsDivididasSeg[h][0][j]
                
                treinoListaMaligno = foldsDivididasM[h][0][j]
                treinoListaMalignoSeg = foldsDivididasSegM[h][0][j]
            else:
                treinoListaBenigno =treinoListaBenigno+ foldsDivididas[h][0][j]
                treinoListaBenignoSeg = treinoListaBenignoSeg+ foldsDivididasSeg[h][0][j]
                
                treinoListaMaligno = treinoListaMaligno+ foldsDivididasM[h][0][j]
                treinoListaMalignoSeg = treinoListaMalignoSeg+ foldsDivididasSegM[h][0][j]
                
                
          #-----------------carregando imagens benignas----------------#  
        #carregando treino benigno
        for i in range(int(len(treinoListaBenigno))):    
            reaisBa,labelsReaisBa = carregarImagens(str(treinoListaBenigno[i]),height, width,0)
            segaux,lixo = carregarImagens(str(treinoListaBenignoSeg[i]),height, width,0)
            if i > 0:
               nB =  np.concatenate([nB,reaisBa])
               seg_train_b = np.concatenate([seg_train_b,segaux])
               lnB = np.concatenate([lnB ,labelsReaisBa])
            else:
                seg_train_b= segaux
                nB =reaisBa
                lnB = labelsReaisBa
        for i in range(len(testeListaBenigno)):    
            reaisBa,labelsReaisBa = carregarImagens(str(testeListaBenigno[i]),height, width,0)
            if i > 0:
               nB2 =  np.concatenate([nB2,reaisBa])
               lnB2= np.concatenate([lnB2,labelsReaisBa])
            else:
                nB2 =reaisBa
                lnB2= labelsReaisBa
        for i in range(len(validacaoListaBenigno)):    
            reaisBa,labelsReaisBa = carregarImagens(str(validacaoListaBenigno[i]),height, width,0)
            if i > 0:
               validacaoB =  np.concatenate([validacaoB,reaisBa])
               labelsValB= np.concatenate([labelsValB,labelsReaisBa])
            else:
                validacaoB =reaisBa
                labelsValB= labelsReaisBa
                
    #-----------------carregando imagens malignas----------------#



        #carregando treino maligno
        for i in range(int(len(treinoListaMaligno))):    
            reaisBa,labelsReaisBa = carregarImagens(str(treinoListaMaligno[i]),height, width,1)
            segaux,lixo = carregarImagens(str(treinoListaMalignoSeg[i]),height, width,1)
            if i > 0:
               nM =  np.concatenate([nM,reaisBa])
               seg_train_b = np.concatenate([seg_train_m,segaux])
               lnM = np.concatenate([lnM ,labelsReaisBa])
            else:
                seg_train_m= segaux
                nM =reaisBa
                lnM = labelsReaisBa
        

   
        #carregando teste maligno
        for i in range(len(testeListaMaligno)):    
            reaisBa,labelsReaisBa = carregarImagens(str(testeListaMaligno[i]),height, width,1)
            if i > 0:
               nM2 =  np.concatenate([nM2,reaisBa])
               lnM2= np.concatenate([lnM2,labelsReaisBa])
            else:
                nM2 =reaisBa
                lnM2= labelsReaisBa

    

        for i in range(len(validacaoListaMaligno)):    
            reaisBa,labelsReaisBa = carregarImagens(str(validacaoListaMaligno[i]),height, width,1)
            if i > 0:
               validacaoM =  np.concatenate([validacaoM,reaisBa])
               labelsValM= np.concatenate([labelsValM,labelsReaisBa])
            else:
                validacaoM =reaisBa
                labelsValM= labelsReaisBa
                
                
        if len(validacaoB) <= menorvali :
            menorvali = len(validacaoB)
        if len(nB2) <= menorTeste :
            menorTeste = len(nB2)
        if len(nB) <= menortreino :
            menortreino = len(nB)

        if len(validacaoM) <= menorvaliM :
            menorvaliM = len(validacaoM)
        if len(nM2) <= menorTesteM :
            menorTesteM = len(nM2)
        if len(nM) <= menortreinoM :
            menortreinoM = len(nM)
#_____________________
    for h in range(folds):
        testeListaBenigno = foldsDivididas[h][1]

        testeListaMaligno = foldsDivididasM[h][1]
        for j in range(folds-1):
            if j==0:
                validacaoListaBenigno = foldsDivididas[h][0][j]

                validacaoListaMaligno = foldsDivididasM[h][0][j]
            elif j==1: 
                treinoListaBenigno = foldsDivididas[h][0][j]
                treinoListaBenignoSeg = foldsDivididasSeg[h][0][j]

                treinoListaMaligno = foldsDivididasM[h][0][j]
                treinoListaMalignoSeg = foldsDivididasSegM[h][0][j]
            else:
                treinoListaBenigno =treinoListaBenigno+ foldsDivididas[h][0][j]
                treinoListaBenignoSeg = treinoListaBenignoSeg+ foldsDivididasSeg[h][0][j]

                treinoListaMaligno = treinoListaMaligno+ foldsDivididasM[h][0][j]
                treinoListaMalignoSeg = treinoListaMalignoSeg+ foldsDivididasSegM[h][0][j]


          #-----------------carregando imagens benignas----------------#  
        #carregando treino benigno
        for i in range(int(len(treinoListaBenigno))):    
            reaisBa,labelsReaisBa = carregarImagens(str(treinoListaBenigno[i]),height, width,0)
            segaux,lixo = carregarImagens(str(treinoListaBenignoSeg[i]),height, width,0)
            if i > 0 :
               nB =  np.concatenate([nB,reaisBa])
               seg_train_b = np.concatenate([seg_train_b,segaux])
               lnB = np.concatenate([lnB ,labelsReaisBa])
            else:
                seg_train_b= segaux
                nB =reaisBa
                lnB = labelsReaisBa
        for i in range(len(testeListaBenigno)):    
            reaisBa,labelsReaisBa = carregarImagens(str(testeListaBenigno[i]),height, width,0)
            if i > 0 :
               nB2 =  np.concatenate([nB2,reaisBa])
               lnB2= np.concatenate([lnB2,labelsReaisBa])
            else:
                nB2 =reaisBa
                lnB2= labelsReaisBa
        
        nB2 = shuffle(nB2, random_state=1)
        nB2 = nB2[:menorTeste]
        lnB2 = lnB2[:menorTeste]
            
        for i in range(len(validacaoListaBenigno)):    
            reaisBa,labelsReaisBa = carregarImagens(str(validacaoListaBenigno[i]),height, width,0)
            if i > 0 :
               validacaoB =  np.concatenate([validacaoB,reaisBa])
               labelsValB= np.concatenate([labelsValB,labelsReaisBa])
            else:
                validacaoB =reaisBa
                labelsValB= labelsReaisBa


        #-----------------carregando imagens malignas----------------#



        #carregando treino maligno
        for i in range(int(len(treinoListaMaligno))):    
            reaisBa,labelsReaisBa = carregarImagens(str(treinoListaMaligno[i]),height, width,1)
            segaux,lixo = carregarImagens(str(treinoListaMalignoSeg[i]),height, width,1)
            if i > 0 :
               nM =  np.concatenate([nM,reaisBa])
               seg_train_b = np.concatenate([seg_train_m,segaux])
               lnM = np.concatenate([lnM ,labelsReaisBa])
            else:
                seg_train_m= segaux
                nM =reaisBa
                lnM = labelsReaisBa



        #carregando teste maligno
        for i in range(len(testeListaMaligno)):    
            reaisBa,labelsReaisBa = carregarImagens(str(testeListaMaligno[i]),height, width,1)
            if i > 0 :

               nM2 =  np.concatenate([nM2,reaisBa])
    
               lnM2= np.concatenate([lnM2,labelsReaisBa])
            else:
                nM2 =reaisBa
                lnM2= labelsReaisBa
        nM2 = shuffle(nM2, random_state=1)
        nM2 = nM2[:menorTesteM]
        lnM2 = lnM2[:menorTesteM]


        for i in range(len(validacaoListaMaligno)):    
            reaisBa,labelsReaisBa = carregarImagens(str(validacaoListaMaligno[i]),height, width,1)
            if i > 0:
               validacaoM =  np.concatenate([validacaoM,reaisBa])
               labelsValM= np.concatenate([labelsValM,labelsReaisBa])
            else:
                validacaoM =reaisBa
                labelsValM= labelsReaisBa

 
        #---------Salvando configuracao das folds-----------#
        file = open(mainPath+"/Config Por Folds.txt","a+") 
        file.write("Fold: "+str(h))
        file.write("\n")
        file.write("Benigno Nodulos: Treino - "+str(len(treinoListaBenigno))+"; Validacao - "+str(len(validacaoListaBenigno))+"; Teste - "+str(len(testeListaBenigno)))
        file.write("\n")
        file.write("Benigno Cortes: Treino - "+str(len(nB))+"; Validacao - "+str(len(validacaoB))+"; Teste - "+str(len(nB2)))
        file.write("\n")
        file.write("Maligno Nodulos: Treino - "+str(len(treinoListaMaligno))+"; Validacao - "+str(len(validacaoListaMaligno))+"; Teste - "+str(len(testeListaMaligno)))
        file.write("\n")
        file.write("Maligno Cortes: Treino - "+str(len(nM))+"; Validacao - "+str(len(validacaoM))+"; Teste - "+str(len(nM2)))
        file.write("\n")
        file.write("-----------------------------------")
        file.write("\n")
        file.close() 
        
         #------------crinado Arg Classico-----------#
        
        nB = nB.reshape((nB.shape[0],) +(height,width, channels)).astype('float32') / 255.
        seg_train_b = seg_train_b.reshape((seg_train_b.shape[0],) +(height,width, channels)).astype('float32') / 255.
        Arg = dataArg(nB,10,1000)
        for o in range(InteracaiArgClassic):
            if o==0:
                geradorImagensClassic = Arg.flow(nB, batch_size=1)
                dataArgB = geradorImagensClassic.next()
            else:
                geradorImagensClassic = Arg.flow(nB, batch_size=1)
                dataArgB = np.concatenate([dataArgB,geradorImagensClassic.next()])
            for c in range(len(geradorImagensClassic)-1):
                dataArgB = np.concatenate([dataArgB,geradorImagensClassic.next()])

        nM = nM.reshape((nM.shape[0],) +(height,width, channels)).astype('float32') / 255.
        seg_train_m = seg_train_m.reshape((seg_train_m.shape[0],) +(height,width, channels)).astype('float32') / 255.
        Arg = dataArg(nM,10,1000)
        for o in range(InteracaiArgClassic):
            if o==0:
                geradorImagensClassic = Arg.flow(nM, batch_size=1)
                dataArgM = geradorImagensClassic.next()
            else:
                geradorImagensClassic = Arg.flow(nM, batch_size=1)
                dataArgM = np.concatenate([dataArgM,geradorImagensClassic.next()])
            for c in range(len(geradorImagensClassic)-1):
                dataArgM = np.concatenate([dataArgM,geradorImagensClassic.next()])
        file = open(mainPath+"/Config Tamanho Aumento.txt","a+") 
        file.write("Fold: "+str(h))
        file.write("\n")
    
        file.write("Benigno Cortes: aumento - "+ str(len(dataArgB)))
        file.write("\n")
        file.write("Maligno Cortes: aumento - "+ str(len(dataArgM)))
        file.write("\n")
        file.write("-----------------------------------")
        file.write("\n")
        file.close() 
            
        #---------------------executando Aumento Classico----------------#
        fp,tp,auc = runArgclassic(h,nB,nB2,lnB,lnB2,validacaoB,labelsValB,nM,nM2,lnM,lnM2,validacaoM,labelsValM,dataArgM,dataArgB,"Classic")
        fp1.append(fp)
        tp1.append(tp)
        auc1.append(auc)
        #---------------- treinando AnjosGAN----------------------#
        aumentoM = len(dataArgM)
        aumentoB = len(dataArgB)
        
        ganB,ganM = AnjosGAN(np.concatenate([nB,dataArgB]),seg_train_b,np.concatenate([nM,dataArgM]),seg_train_m,aumentoB,aumentoM)
        
        fp,tp,auc = runArgclassic(h,nB,nB2,lnB,lnB2,validacaoB,labelsValB,nM,nM2,lnM,lnM2,validacaoM,labelsValM,ganM,ganB,"AnjosGAN")
        fp2.append(fp)
        tp2.append(tp)
        auc2.append(auc)
        
        #criar pasta para cada fold
        save_dir = mainPath+'/Amostrages Anjos'
        if os.path.isdir(save_dir): # vemos de este diretorio ja existe
            print("pasta ja existe")
        else:
            print("Criando Pasta")
            os.mkdir(save_dir)
            
        print("Gerando Imagens")
        for v in range(10):
            img = image.array_to_img(ganB[v] * 255., scale=False)
            img.save(os.path.join(save_dir+"/",str(h)+"AnjosnoduloB" + str(v) + '.png')) 
            img = image.array_to_img(ganM[v] * 255., scale=False)
            img.save(os.path.join(save_dir+"/", str(h)+"AnjosnoduloM" + str(v) + '.png'))

        #----------------------treinando Classic + AnjosGAN--------------------# 
        
        fp,tp,auc = runArgclassic(h,nB,nB2,lnB,lnB2,validacaoB,labelsValB,nM,nM2,lnM,lnM2,validacaoM,labelsValM,np.concatenate([ganM,dataArgM]),np.concatenate([ganB,dataArgB]),afim+" + AnjosGAN")
        fp3.append(fp)
        tp3.append(tp)
        auc3.append(auc)
                                                 
        #----------------------treinando DCGAN--------------------# 
        ganB,ganM = DCGAN(np.concatenate([nB,dataArgB]),np.concatenate([nM,dataArgM]),aumentoB,aumentoM)
        fp,tp,auc = runArgclassic(h,nB,nB2,lnB,lnB2,validacaoB,labelsValB,nM,nM2,lnM,lnM2,validacaoM,labelsValM,ganM,ganB,dcgan)
        fp4.append(fp)
        tp4.append(tp)
        auc4.append(auc)
        save_dir = mainPath+'/Amostrages DCGAN'
        if os.path.isdir(save_dir): # vemos de este diretorio ja existe
            print("pasta ja existe")
        else:
            print("Criando Pasta")
            os.mkdir(save_dir)
            
        print("Gerando Imagens")
        for v in range(10):
            img = image.array_to_img(ganB[v] * 255., scale=False)
            img.save(os.path.join(save_dir+"/",str(h)+"DCGANsnoduloB" + str(v) + '.png')) 
            img = image.array_to_img(ganM[v] * 255., scale=False)
            img.save(os.path.join(save_dir+"/", str(h)+"DCGANnoduloM" + str(v) + '.png'))
                                                 
        #----------------------treinando Classic + DCGAN--------------------# 
        fp,tp,auc = runArgclassic(h,nB,nB2,lnB,lnB2,validacaoB,labelsValB,nM,nM2,lnM,lnM2,validacaoM,labelsValM,np.concatenate([ganM,dataArgM]),np.concatenate([ganB,dataArgB]),afim+" + "+dcgan)
        fp5.append(fp)
        tp5.append(tp)
        auc5.append(auc)
                                                 
        #----------------------treinando WGAN--------------------# 
        ganB,ganM = WGAN(np.concatenate([nB,dataArgB]),np.concatenate([nM,dataArgM]),aumentoB,aumentoM)
        fp,tp,auc = runArgclassic(h,nB,nB2,lnB,lnB2,validacaoB,labelsValB,nM,nM2,lnM,lnM2,validacaoM,labelsValM,ganM,ganB,wgan)
        fp6.append(fp)
        tp6.append(tp)
        auc6.append(auc)
        save_dir = mainPath+'/Amostrages WGAN'
        if os.path.isdir(save_dir): # vemos de este diretorio ja existe
            print("pasta ja existe")
        else:
            print("Criando Pasta")
            os.mkdir(save_dir)
            
        print("Gerando Imagens")
        for v in range(10):
            img = image.array_to_img(ganB[v] * 255., scale=False)
            img.save(os.path.join(save_dir+"/",str(h)+"WGANnoduloB" + str(v) + '.png')) 
            img = image.array_to_img(ganM[v] * 255., scale=False)
            img.save(os.path.join(save_dir+"/", str(h)+"WGANnoduloM" + str(v) + '.png'))

        #----------------------treinando Classic + WGAN--------------------# 
                                                 
        fp,tp,auc = runArgclassic(h,nB,nB2,lnB,lnB2,validacaoB,labelsValB,nM,nM2,lnM,lnM2,validacaoM,labelsValM,np.concatenate([ganM,dataArgM]),np.concatenate([ganB,dataArgB]),afim+" + "+wgan)
        fp7.append(fp)
        tp7.append(tp)
        auc7.append(auc)
        
        
        
#------------------Graficos--------------#
#Afim

    plt.figure(1)
    plt.plot([0, 1], [0, 1], 'k--')
    file = open(mainPath+"/AfinsAUC.txt","a+")
    for t in range(folds):
        file.write("$fp$"+ str(fp1[t]))
        file.write("\n")
        file.write("$tp$"+ str(tp1[t]))
        file.write("\n")
        file.write("$tp$"+ str(auc1[t]))
        plt.plot(fp1[t], tp1[t], label='Classic '+str(t)+' (area = {:.3f})'.format(auc1[t]))
        file.write("\n")
        file.write("-----------------------------------")
        file.write("\n")
    file.close()
    
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve')
    plt.legend(loc='best')
    plt.savefig(mainPath+"/AfimJuntos")
    plt.show()
    
    
#Anjos
        
    plt.figure(1)
    plt.plot([0, 1], [0, 1], 'k--')
    file = open(mainPath+"/AnjosAUC.txt","a+")
    for t in range(folds):
        plt.plot(fp2[t], tp2[t], label='AnjosGAN '+str(t)+' (area = {:.3f})'.format(auc2[t]))
        file.write("$fp$"+ str(fp2[t]))
        file.write("\n")
        file.write("$tp$"+ str(tp2[t]))
        file.write("\n")
        file.write("$tp$"+ str(auc2[t]))
        file.write("\n")
        file.write("-----------------------------------")
        file.write("\n")
    file.close()
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve')
    plt.legend(loc='best')
    plt.savefig(mainPath+"/AnjosGANJuntos")
    plt.show()
    
#Anjos + Afim

    plt.figure(1)
    plt.plot([0, 1], [0, 1], 'k--')
    file = open(mainPath+"/AnjosClassicAUC.txt","a+")
    for t in range(folds):
        
        plt.plot(fp3[t], tp3[t], label=afim+' +AnjosGAN '+str(t)+' (area = {:.3f})'.format(auc3[t]))
        file.write("$fp$"+ str(fp3[t]))
        file.write("\n")
        file.write("$tp$"+ str(tp3[t]))
        file.write("\n")
        file.write("$tp$"+ str(auc3[t]))
        file.write("\n")
        file.write("-----------------------------------")
        file.write("\n")
    file.close()
    
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve')
    plt.legend(loc='best')
    plt.savefig(mainPath+"/AnjosGAN+afimJuntos")
    plt.show()
    
#DCGAN

    plt.figure(1)
    plt.plot([0, 1], [0, 1], 'k--')
    file = open(mainPath+"/DcGANAUC.txt","a+")
    for t in range(folds):
        
        plt.plot(fp4[t], tp4[t], label=dcgan+' '+str(t)+' (area = {:.3f})'.format(auc4[t]))
        file.write("$fp$"+ str(fp4[t]))
        file.write("\n")
        file.write("$tp$"+ str(tp4[t]))
        file.write("\n")
        file.write("$tp$"+ str(auc4[t]))
        file.write("\n")
        file.write("-----------------------------------")
        file.write("\n")
    file.close()
    
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve')
    plt.legend(loc='best')
    plt.savefig(mainPath+"/DCGANJuntos")
    plt.show()
    
#DCGAN + AFIM

    plt.figure(1)
    plt.plot([0, 1], [0, 1], 'k--')
    file = open(mainPath+"/DcGANClassicAUC.txt","a+")
    for t in range(folds):
        
        plt.plot(fp5[t], tp5[t], label=afim+' +  '+dcgan+' '+str(t)+' (area = {:.3f})'.format(auc5[t]))
        file.write("$fp$"+ str(fp5[t]))
        file.write("\n")
        file.write("$tp$"+ str(tp5[t]))
        file.write("\n")
        file.write("$tp$"+ str(auc5[t]))
        file.write("\n")
        file.write("-----------------------------------")
        file.write("\n")
    file.close()
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve')
    plt.legend(loc='best')
    plt.savefig(mainPath+"/DCGAN+AFIMJuntos")
    plt.show()
    
#WGAN

    plt.figure(1)
    plt.plot([0, 1], [0, 1], 'k--')
    file = open(mainPath+"/WGANAUC.txt","a+")
    for t in range(folds):
        
        plt.plot(fp6[t], tp6[t], label=wgan+' '+str(t)+' (area = {:.3f})'.format(auc6[t]))
        file.write("$fp$"+ str(fp6[t]))
        file.write("\n")
        file.write("$tp$"+ str(tp6[t]))
        file.write("\n")
        file.write("$tp$"+ str(auc6[t]))
        file.write("\n")
        file.write("-----------------------------------")
        file.write("\n")
    file.close()
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve')
    plt.legend(loc='best')
    plt.savefig(mainPath+"/WGANJuntos")
    plt.show()
    
#WGAN + AFIM

    plt.figure(1)
    plt.plot([0, 1], [0, 1], 'k--')
    file = open(mainPath+"/WGANclassicAUC.txt","a+")
    for t in range(folds):
        
        plt.plot(fp7[t], tp7[t], label=afim+' '+wgan+' '+str(t)+' (area = {:.3f})'.format(auc7[t]))
        file.write("$fp$"+ str(fp7[t]))
        file.write("\n")
        file.write("$tp$"+ str(tp7[t]))
        file.write("\n")
        file.write("$tp$"+ str(auc7[t]))
        file.write("\n")
        file.write("-----------------------------------")
        file.write("\n")
    file.close()
    plt.xlabel('False positive rate')
    plt.ylabel('True positive rate')
    plt.title('ROC curve')
    plt.legend(loc='best')
    plt.savefig(mainPath+"/WGAN+AFIMJuntos")
    plt.show()
    for t in range(folds):
        plt.figure(1)
        plt.plot(fp1[t], tp1[t], label='Classic '+str(t)+' (area = {:.3f})'.format(auc1[t]))
        plt.plot(fp2[t], tp2[t], label='AnjosGAN '+str(t)+' (area = {:.3f})'.format(auc2[t]))
        plt.plot(fp3[t], tp3[t], label=afim+' +AnjosGAN '+str(t)+' (area = {:.3f})'.format(auc3[t]))
        plt.plot(fp4[t], tp4[t], label=dcgan+' '+str(t)+' (area = {:.3f})'.format(auc4[t]))
        plt.plot(fp5[t], tp5[t], label=afim+' +  '+dcgan+' '+str(t)+' (area = {:.3f})'.format(auc5[t]))
        plt.plot(fp6[t], tp6[t], label=wgan+' '+str(t)+' (area = {:.3f})'.format(auc6[t]))
        plt.plot(fp7[t], tp7[t], label=afim+' '+wgan+' '+str(t)+' (area = {:.3f})'.format(auc7[t]))
        plt.xlabel('False positive rate')
        plt.ylabel('True positive rate')
        plt.title('ROC curve')
        plt.legend(loc='best')
        plt.savefig(mainPath+"/JuntosFolds"+str(t))
        plt.show()
        
    for t in range(folds):
        plt.figure(1)
        plt.plot(fp1[t], tp1[t], label='Classic '+str(t)+' (area = {:.3f})'.format(auc1[t]))
        #plt.plot(fp2[t], tp2[t], label='AnjosGAN '+str(t)+' (area = {:.3f})'.format(auc2[t]))
        plt.plot(fp3[t], tp3[t], label=afim+' +AnjosGAN '+str(t)+' (area = {:.3f})'.format(auc3[t]))
       # plt.plot(fp4[t], tp4[t], label=dcgan+' '+str(t)+' (area = {:.3f})'.format(auc4[t]))
        plt.plot(fp5[t], tp5[t], label=afim+' +  '+dcgan+' '+str(t)+' (area = {:.3f})'.format(auc5[t]))
      #  plt.plot(fp6[t], tp6[t], label=wgan+' '+str(t)+' (area = {:.3f})'.format(auc6[t]))
        plt.plot(fp7[t], tp7[t], label=afim+' '+wgan+' '+str(t)+' (area = {:.3f})'.format(auc7[t]))
        plt.xlabel('False positive rate')
        plt.ylabel('True positive rate')
        plt.title('ROC curve')
        plt.legend(loc='best')
        plt.savefig(mainPath+"/JuntosClassiFolds"+str(t))
        plt.show()
    for t in range(folds):
        plt.figure(1)
        plt.plot(fp1[t], tp1[t], label='Classic '+str(t)+' (area = {:.3f})'.format(auc1[t]))
        plt.plot(fp2[t], tp2[t], label='AnjosGAN '+str(t)+' (area = {:.3f})'.format(auc2[t]))
        #plt.plot(fp3[t], tp3[t], label=afim+' +AnjosGAN '+str(t)+' (area = {:.3f})'.format(auc3[t]))
        plt.plot(fp4[t], tp4[t], label=dcgan+' '+str(t)+' (area = {:.3f})'.format(auc4[t]))
       # plt.plot(fp5[t], tp5[t], label=afim+' +  '+dcgan+' '+str(t)+' (area = {:.3f})'.format(auc5[t]))
        plt.plot(fp6[t], tp6[t], label=wgan+' '+str(t)+' (area = {:.3f})'.format(auc6[t]))
      #  plt.plot(fp7[t], tp7[t], label=afim+' '+wgan+' '+str(t)+' (area = {:.3f})'.format(auc7[t]))
        plt.xlabel('False positive rate')
        plt.ylabel('True positive rate')
        plt.title('ROC curve')
        plt.legend(loc='best')
        plt.savefig(mainPath+"/JuntosIndividualFolds"+str(t))
        plt.show()
    
    return 0
        
#-------------------------------------Média-------------#



xecutar(listaBenigno,listaSegmentados,listaMaligno,listaSegmentadosM,folds)
